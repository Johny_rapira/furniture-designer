﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ChooseControl : MonoBehaviour {
    private GameObject listPrefabTop;
    private GameObject listPrefabBottom;
    private GameObject parentTarget;
    private GameObject panelSize;
    private Transform target;
    private int countPrefabTop = 0;
    private int countPrefabBottom = 0;
    private float limitS;
    private float limitE;
    private bool a = false;
    private bool state = false;
    private int _width;
    private GameObject[] arrayOfPrefabs;

    public Button Back;

    void Start()
    {
        Back.onClick.AddListener(onClickBack);
        panelSize = GameObject.Find("PanelSize");
        GameObject.Find("TopModule").GetComponent<Button>().onClick.AddListener(onClickTopModule);
        GameObject.Find("BottomModule").GetComponent<Button>().onClick.AddListener(onClickBottomModule);
        listPrefabTop = GameObject.Find("ListPrefabTop");
        listPrefabBottom = GameObject.Find("ListPrefabBottom");
        panelSize.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(onClickReduce);
        panelSize.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(onClickIncrease);
        panelSize.transform.GetChild(3).GetComponent<Button>().onClick.AddListener(onClickFinish);
        panelSize.SetActive(false);
        listPrefabBottom.SetActive(false);
        limitS = listPrefabTop.transform.position.y;

        arrayOfPrefabs = Resources.LoadAll<GameObject>("Prefab");
        for(int i=0;i<arrayOfPrefabs.Length;i++)
        {
            if (arrayOfPrefabs[i].name.IndexOf("[Top]") != -1)
            {
                Transform t = Instantiate(arrayOfPrefabs[i], listPrefabTop.transform).transform;
                t.position = new Vector3(t.parent.position.x, t.parent.position.y - countPrefabTop * 1.8f, 0f);
                t.localScale = new Vector3(1.3f, 1.3f, 1.3f);
                countPrefabTop++;
            }
            else if (arrayOfPrefabs[i].name.IndexOf("[Bottom]") != -1)
            {
                Transform t = Instantiate(arrayOfPrefabs[i], listPrefabBottom.transform).transform;
                t.position = new Vector3(t.parent.position.x, t.parent.position.y - countPrefabBottom * 1.8f, 0f);
                t.localScale = new Vector3(1.3f, 1.3f, 1.3f);
                countPrefabBottom++;
            }
        }
        limitE = limitS + (arrayOfPrefabs.Length > 6 ? (arrayOfPrefabs.Length - 6) * 2.1f + 0.9f : 0f);
	}
    void Update()
    {
        if (Input.touchCount > 0)
        {
            switch (Input.GetTouch(0).phase)
            {
                case TouchPhase.Began:
                    if (!state)
                    {
                        RayStrike();
                        a = true;
                    }
                    break;
                case TouchPhase.Moved:
                    if (parentTarget && !state)
                    {
                        a = false;
                        float y = listPrefabTop.transform.position.y + Input.GetTouch(0).deltaPosition.y * Time.deltaTime;
                        if(y>=limitS && y<=limitE)
                            listPrefabTop.transform.position += new Vector3(0, Input.GetTouch(0).deltaPosition.y * Time.deltaTime *0.6f);
                    }
                    break;
                case TouchPhase.Ended:
                    #region change_state
                    if (!state)
                    {
                        parentTarget = null;
                        if (a && target != null)
                        {
                            state = true;
                            Destroy(listPrefabTop.gameObject);
                            Destroy(listPrefabBottom.gameObject);
                            Destroy(GameObject.Find("TopModule"));
                            Destroy(GameObject.Find("BottomModule"));

                            string module_name = target.name.Replace("(Clone)", "");
                            target = Instantiate<GameObject>(Resources.Load<GameObject>("Prefab/" + module_name)).transform;
                            target.position = new Vector3(0, 2.5f, 0);
                            target.localScale = new Vector3(3, 3, 3);
                            panelSize.SetActive(true);
                            panelSize.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(onClickReduce);
                            panelSize.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(onClickIncrease);
                            panelSize.transform.GetChild(3).GetComponent<Button>().onClick.AddListener(onClickFinish);
                            Text text = panelSize.transform.GetChild(2).GetComponent<Text>();
                            switch (module_name)
                            {
                                case "[Bottom]Module1":
                                    _width = 400;
                                    break;
                            }

                            text.text = _width.ToString();
                            /*
                            ModuleController.instance = target.name.Replace("(Clone)", "");
                            ModuleController.isTopModule = listPrefabTop.activeInHierarchy;
                            SceneManager.LoadScene(2);
                             */

                        }
                    }
                    break;
                    #endregion
            }
        }
	}
    #region UI_controll
    void onClickReduce()
    {
        Text text = panelSize.transform.GetChild(2).GetComponent<Text>();
        int width = int.Parse(text.text) - 4;
        target.localScale = new Vector3(width*3.0f/_width,target.localScale.y);
        text.text = width.ToString();
    }
    void onClickIncrease()
    {
        Text text = panelSize.transform.GetChild(2).GetComponent<Text>();
        int width = int.Parse(text.text) + 4;
        target.localScale = new Vector3(width * 3.0f / _width, target.localScale.y,target.localScale.z);
        text.text = width.ToString();
    }
    void onClickFinish()
    {
        Text text = panelSize.transform.GetChild(2).GetComponent<Text>();
        ModuleController.ObjectToInstance = new ModuleController.SaveObject();
        ModuleController.ModuleAddition addition = new ModuleController.ModuleAddition();
        ModuleController.ObjectToInstance.NamePrefab = target.name.Replace("(Clone)", "");
        ModuleController.ObjectToInstance.Scale = target.localScale - Vector3.one * 2;
        Destroy(target.gameObject);

        #region raskroi
        float scale = _width / int.Parse(text.text);
        switch (ModuleController.ObjectToInstance.NamePrefab)
        {
            case "[Bottom]Module1":
                addition.bot = FillArray(new Vector2(450 * scale, 500), 1);
                addition.side = FillArray(new Vector2(704, 500), 2);
                addition.bar = FillArray(new Vector2(418 * scale, 80), 2);
                addition.shelf = FillArray(new Vector2(418 * scale, 500), 1);
                addition.back_wall = FillArray(new Vector2(716, 446 * scale), 1);
                addition.facade = FillArray(new Vector2(716, 446), 1);
                addition.handle = 1;
                addition.legs = 4;
                addition.buttonhole = 2;
                addition.shelf_support = 4;
                break;
        }
        #endregion

        ModuleController.ObjectToInstance.Addintion = addition;
        SceneManager.LoadScene(2);
    }
    void onClickTopModule()
    {
        listPrefabBottom.SetActive(false);
        listPrefabTop.SetActive(true);
        limitE = limitS + (countPrefabTop > 6 ? (countPrefabTop - 6) * 2.1f + 0.9f : 0f);
    }
    void onClickBottomModule()
    {
        listPrefabTop.SetActive(false);
        listPrefabBottom.SetActive(true);
        limitE = limitS + (countPrefabBottom > 6 ? (countPrefabBottom - 6) * 2.1f + 0.9f : 0f);
    }
    void onClickBack()
    {
        ModuleController.EnablePrefabs();
        SceneManager.LoadScene(2);
    }
    #endregion
    #region under
    Vector2[] FillArray(Vector2 v, int n)
    {
        Vector2[] r = new Vector2[n];
        for (int i = 0; i < n; i++)
            r[i] = v;
        return r;
    }

    bool RayStrike()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            Transform superParent = hit.transform;
            Transform t;
            while ((t = superParent.parent) != null) superParent = t;

            if (superParent.gameObject.name.Equals("ListPrefabTop") || superParent.gameObject.name.Equals("ListPrefabBottom"))
            {
                parentTarget = hit.transform.gameObject;
                target = hit.transform;
                while (!target.tag.Equals("Module"))
                {
                    target = target.parent;
                }
            }
        }
        return false;
    }
    #endregion

}
