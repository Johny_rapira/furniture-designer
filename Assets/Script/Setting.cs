﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Setting : MonoBehaviour {
    public static string FACADE_TOP;
    public static string FACADE_BOTTOM;
    public static string COUNTERTOP;
    public static string FRAME;

	void Start () {
        DontDestroyOnLoad(this);
	}
}
