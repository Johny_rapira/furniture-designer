﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ReportControl : MonoBehaviour {
    public Transform Panel;
    
	void Start () {
        // Ручки
        Panel.GetChild(0).GetComponent<Text>().text += ModuleController.GetHandles();
        // Ножки
        Panel.GetChild(1).GetComponent<Text>().text += ModuleController.GetLegs();
        // Петли
        Panel.GetChild(2).GetComponent<Text>().text += ModuleController.GetButtonHoles();
        // Полкодержатель
        Panel.GetChild(3).GetComponent<Text>().text += ModuleController.GetShelfSupports();
        // Кромка
        Panel.GetChild(4).GetComponent<Text>().text += ModuleController.GetEdges();
        // Вернутся
        Panel.GetChild(5).GetComponent<Button>().onClick.AddListener(onClickBack);
	}

    private void onClickBack()
    {
        ModuleController.EnablePrefabs();
        SceneManager.LoadScene(2);
    }
	
	void Update () {
		
	}
}
