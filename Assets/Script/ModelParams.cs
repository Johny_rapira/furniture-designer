﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class ModelParams : MonoBehaviour {
    public Vector3 StartPosition;
    public ModuleController.SaveObject instance;

    void Start()
    {
        DontDestroyOnLoad(gameObject);
        if (SceneManager.GetActiveScene().buildIndex == 2)
        {
            StartPosition += new Vector3((instance.Scale.x-1)/2, 0);
            transform.position = StartPosition;
            transform.localScale = instance.Scale;

            int count = transform.childCount;
            Material ft = Resources.Load<Material>("Materials/" + Setting.FACADE_TOP);
            Material fb = Resources.Load<Material>("Materials/" + Setting.FACADE_BOTTOM);
            Material f = Resources.Load<Material>("Materials/" + Setting.FRAME);
            Material c = Resources.Load<Material>("Materials/" + Setting.COUNTERTOP);
            for (int i = 0; i < count; i++)
            {
                Transform trans = transform.GetChild(i);
                switch (trans.tag)
                {
                    case "FacadeTop":
                        trans.GetComponent<Renderer>().material = ft;
                        break;
                    case "FacadeBottom":
                        trans.GetComponent<Renderer>().material = fb;
                        break;
                    case "CounterBoard":
                        trans.GetComponent<Renderer>().material = c;
                        break;
                    case "ModuleElement":
                        trans.GetComponent<Renderer>().material = f;
                        break;
                }
            }

        }
        
	}
}
