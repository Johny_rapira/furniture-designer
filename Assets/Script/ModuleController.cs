﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModuleController : MonoBehaviour {
    public struct ModuleAddition
    {
        public Vector2[] bot;
        public Vector2[] side;
        public Vector2[] bar;
        public Vector2[] shelf;
        public Vector2[] back_wall;
        public Vector2[] facade;
        public int handle;
        public int legs;
        public int buttonhole;
        public int shelf_support;
        public int edge;
    }
    public class SaveObject{
        public string NamePrefab;
        public Vector3 Scale;
        public ModuleAddition Addintion;
    }

    private static GameObject[] Instance;
    public static SaveObject ObjectToInstance = null;

    void Start()
    {
        DontDestroyOnLoad(this);
    }
	void Awake() {
        //EnablePrefabs();
        AddModule();
	}

    #region getters
    public static int GetHandles()
    {
        int result = 0;
        if (Instance == null) return 0;
        foreach (GameObject obj in Instance)
            result += obj.GetComponent<ModelParams>().instance.Addintion.handle;
        Debug.Log(result);
        return result;
    }
    public static int GetLegs()
    {
        int result = 0;
        if (Instance == null) return 0;
        foreach (GameObject obj in Instance)
            result += obj.GetComponent<ModelParams>().instance.Addintion.legs;
        return result;
    }
    public static int GetButtonHoles()
    {
        int result = 0;
        if (Instance == null) return 0;
        foreach (GameObject obj in Instance)
            result += obj.GetComponent<ModelParams>().instance.Addintion.buttonhole;
        return result;
    }
    public static int GetShelfSupports()
    {
        int result = 0;
        if (Instance == null) return 0;
        foreach (GameObject obj in Instance)
            result += obj.GetComponent<ModelParams>().instance.Addintion.shelf_support;
        return result;
    }
    public static int GetEdges()
    {
        int result = 0;
        if (Instance == null) return 0;
        foreach (GameObject obj in Instance)
            result += obj.GetComponent<ModelParams>().instance.Addintion.edge;
        return result;
    }
    #endregion

    #region controls
    public static void DisableInstance()
    {
        Instance = GameObject.FindGameObjectsWithTag("Module");
        foreach (GameObject obj in Instance)
            obj.SetActive(false);
    }
    public static void EnablePrefabs()
    {
        if(Instance!=null)
        foreach (GameObject obj in Instance)
            obj.SetActive(true);
    }
    public static void AddModule()
    {
        if (ObjectToInstance != null)
        {
            GameObject prefab = Resources.Load<GameObject>("Prefab/" + ObjectToInstance.NamePrefab);
            GameObject instance = Instantiate(prefab);
            instance.GetComponent<ModelParams>().instance = ObjectToInstance;
            ObjectToInstance = null;
        }
    }
    #endregion
}
