﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.UIElements;
using UnityEngine.SceneManagement;

public class MenuControll : MonoBehaviour {
    public Button ButtonKithen;
    public Button ButtonCupboard;
    public Button ButtonCutting;
    public Button ButtonSaveProject;


	void Start () {
        ButtonKithen.onClick.AddListener(onClickKitchen);
        ButtonCupboard.onClick.AddListener(onClickCupboard);
        ButtonCutting.onClick.AddListener(onClickCutting);
        ButtonSaveProject.onClick.AddListener(onClickSaveProject);
	}

    private void onClickSaveProject()
    {
        
    }

    private void onClickCutting()
    {
        throw new System.NotImplementedException();
    }

    private void onClickCupboard()
    {
        throw new System.NotImplementedException();
    }

    private void onClickKitchen()
    {
        SceneManager.LoadScene(1);
    }
	
	void Update () {
		
	}

}
