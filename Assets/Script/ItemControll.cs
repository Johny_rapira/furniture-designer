﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemControll : MonoBehaviour {
    public MenuTexture menu;
    public MenuTexture.eTypes type;

	void Start () {
        transform.GetChild(0).GetComponent<Button>().onClick.AddListener(onClick);
	}

    private void onClick()
    {
        Image image = transform.GetChild(0).GetComponent<Image>();
        menu.MainPanel.transform.GetChild((int)type).GetChild(0).GetComponent<Image>().material = image.material;
        menu.Hide();
    }
}
