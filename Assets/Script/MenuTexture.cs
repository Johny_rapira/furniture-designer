﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuTexture : MonoBehaviour {
    public enum eTypes
    {
        FacadeTop, FacadeBottom, Countertop, Frame
    };

    public GameObject MainPanel;
    public GameObject FacadePanelTop;
    public GameObject FacadePanelBottom;
    public GameObject Countertop;
    public GameObject Frame;

    private GameObject ItemMenu; 
    private GameObject Canvas;

	void Start () {
        ItemMenu = Resources.Load<GameObject>("Prefab/ItemMenu");
        GameObject.Find("Next").GetComponent<Button>().onClick.AddListener(onClickNext);
        MainPanel.transform.GetChild(0).GetChild(1).GetComponent<Button>().onClick.AddListener(onClickFacadeTop);
        MainPanel.transform.GetChild(1).GetChild(1).GetComponent<Button>().onClick.AddListener(onClickFacadeDown);
        MainPanel.transform.GetChild(2).GetChild(1).GetComponent<Button>().onClick.AddListener(onClickCountertop);
        MainPanel.transform.GetChild(3).GetChild(1).GetComponent<Button>().onClick.AddListener(onClickFrame);
        FacadePanelBottom.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(onClickBack);
        FacadePanelTop.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(onClickBack);
        Countertop.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(onClickBack);
        Frame.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(onClickBack);
        for (int i = 0; i < 4; i++)
            FillPanel((eTypes)i);
    }
    private void onClickBack()
    {
        Hide();
    }

    private void onClickNext()
    {
        Setting.FACADE_TOP = MainPanel.transform.GetChild(0).GetChild(0).GetComponent<Image>().material.name;
        Setting.FACADE_BOTTOM = MainPanel.transform.GetChild(1).GetChild(0).GetComponent<Image>().material.name;
        Setting.COUNTERTOP = MainPanel.transform.GetChild(2).GetChild(0).GetComponent<Image>().material.name;
        Setting.FRAME = MainPanel.transform.GetChild(3).GetChild(0).GetComponent<Image>().material.name;
        SceneManager.LoadScene(2);
    }

    private void onClickFrame()
    {
        SetPanel(eTypes.Frame);
    }

    private void onClickCountertop()
    {
        SetPanel(eTypes.Countertop);
    }

    private void onClickFacadeDown()
    {
        SetPanel(eTypes.FacadeBottom);
    }

    private void onClickFacadeTop()
    {
        SetPanel(eTypes.FacadeTop);
    }

    void SetPanel(eTypes type)
    {
        MainPanel.SetActive(false);
        switch (type)
        {
            case eTypes.FacadeTop:
                FacadePanelTop.SetActive(true);
                break;
            case eTypes.FacadeBottom:
                FacadePanelBottom.SetActive(true);
                break;
            case eTypes.Frame:
                Frame.SetActive(true);
                break;
            case eTypes.Countertop:
                Countertop.SetActive(true);
                break;
        }
    }

    void FillPanel( eTypes type)
    {
        Transform panel = null;
        string path = "";
        switch (type) 
        {
            case eTypes.FacadeTop:
                panel = FacadePanelTop.transform;
                path = "Materials/Facade/";
                break;
            case eTypes.FacadeBottom:
                panel = FacadePanelBottom.transform;
                path = "Materials/Facade/";
                break;
            case eTypes.Countertop:
                panel = Countertop.transform;
                path = "Materials/Countertop/";
                break;
            case eTypes.Frame:
                panel = Frame.transform;
                path = "Materials/Frame/";
                break;
            default:
                return;
        }

        Material[] materials = Resources.LoadAll<Material>(path);
        Transform parent = panel.Find("Scroll View/Viewport/Content");


        for (int i = 0; i < materials.Length; i++)
        {
            GameObject item = Instantiate(ItemMenu, parent);
            Image image = item.transform.GetChild(0).GetComponent<Image>();
            Text text = item.transform.GetChild(1).GetComponent<Text>();

            image.material = materials[i];
            text.text = materials[i].name;
            image.material.mainTexture = materials[i].mainTexture;
            ItemControll c = item.GetComponent<ItemControll>();
            c.menu = this;
            c.type = type;
            item.transform.position = new Vector3(item.transform.position.x+(i%4)*140,item.transform.position.y-((int)i/4)*160);
        }
    }
    public void Hide()
    {
        MainPanel.SetActive(true);
        FacadePanelTop.SetActive(false);
        FacadePanelBottom.SetActive(false);
        Frame.SetActive(false);
        Countertop.SetActive(false);
    }
}
