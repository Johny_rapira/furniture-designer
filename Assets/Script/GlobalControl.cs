﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GlobalControl : MonoBehaviour {
    private float distance = 0;
    private Transform transformTarget;
    private GameObject removeTarget;
    private GameObject Prefab;
    private GameObject LeftWall;
    private GameObject RightWall;
    private GameObject Floor;

    public float CameraSpeed = 20;
    public Button ButtonAddModule;
    public Button ButtonRemoveModule;
    public Button ButtonReport;



    void Start()
    {
        LeftWall = GameObject.Find("Wall_Left");
        RightWall = GameObject.Find("Wall_Right");
        Floor = GameObject.Find("Floor");

        ButtonAddModule.onClick.AddListener(AddModule);
        ButtonRemoveModule.onClick.AddListener(RemoveModule);
        ButtonReport.onClick.AddListener(onClickButtonReport);
        ModuleController.AddModule();
	}


    void Update () {
        
        if (Input.touchCount > 0)
        {
            switch (Input.GetTouch(0).phase)
            {
                case TouchPhase.Began:
                    if(Input.touchCount == 1)
                        SetGameObjectRayCast();
                    break;
                case TouchPhase.Moved:
                    if (transformTarget)
                    {
                        if (Input.touchCount > 1)
                            transformTarget = null;
                        else
                            MoveTarget();
                    }
                    else
                    {
                        if (Input.touchCount == 2)
                            Zoom();
                        else if (distance != 0)
                            distance = 0;
                        else if (Input.touchCount == 1)
                            Rotate();
                    }
                    break;
                case TouchPhase.Ended:
                    transformTarget = null;
                    distance = 0;
                    break;
            }
        }
	}

    #region Controll
    private void RemoveModule()
    {
        Destroy(removeTarget);
        transformTarget = null;
        ButtonRemoveModule.interactable = false;
    }
    void AddModule()
    {
        ModuleController.DisableInstance();
        SceneManager.LoadScene(3);
    }
    private void onClickButtonReport()
    {
        ModuleController.DisableInstance();
        SceneManager.LoadScene(4);
    }
    void Rotate()
    {
        Vector2 pos = Input.GetTouch(0).deltaPosition * Time.deltaTime * CameraSpeed;
        float axisX = transform.localRotation.eulerAngles.x;
        transform.Rotate((axisX+pos.y < 80 || axisX+pos.y > 310)?pos.y:0,pos.x*-1,0);
        transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, 0);
        SetActiveGameDecoration();
    }
    void Zoom()
    {
        float dist = Vector3.Distance(Input.GetTouch(0).position,Input.GetTouch(1).position);
        if (distance == 0)
        {
            distance = dist;
            return;
        }
        float delta = (distance - dist);
        transform.position = Vector3.MoveTowards(transform.position, transform.position + transform.forward * -1, Time.deltaTime * delta * CameraSpeed * 0.1f);
        distance = dist;
        SetActiveGameDecoration();
    }
    void SetActiveGameDecoration()
    {
        Floor.SetActive(transform.position.y < -1 ? false : true);
        LeftWall.SetActive(transform.position.x < 0 ? false : true);
        RightWall.SetActive(transform.position.z > 0 ? false : true);
        
    }
    void SetGameObjectRayCast()
    {
            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            RaycastHit hit;
            
            if (Physics.Raycast(ray, out hit))
            {
                Transform superParent = hit.transform;
                Transform t;
                while ((t = superParent.parent) != null) superParent = t;
                if (superParent.tag.Equals("Module"))
                {
                    ButtonRemoveModule.interactable = true;
                    transformTarget = superParent;
                    removeTarget = superParent.gameObject;
                }
            }
    }
    void MoveTarget()
    {
        bool isAxisX = transformTarget.transform.eulerAngles.y == 0;
        Vector3 startPosition = transformTarget.GetComponent<ModelParams>().StartPosition;
        if (isAxisX)
        {
            Vector2 moveVector = new Vector2(Input.GetTouch(0).deltaPosition.x * Input.GetTouch(0).deltaTime * 0.1f, 0);
            transformTarget.Translate(moveVector);
            if (transformTarget.transform.position.x < startPosition.x)
            {
                transformTarget.Rotate(new Vector3(0, -90, 0));
                transformTarget.position.Set(startPosition.z * -1, startPosition.y, startPosition.x * -1);
            }
        }
        else
        {
            Vector3 moveVector = new Vector3(Input.GetTouch(0).deltaPosition.x * Input.GetTouch(0).deltaTime * 0.1f,0);
            transformTarget.Translate(moveVector);
            if (startPosition.z < transformTarget.position.z)
            {
                transformTarget.Rotate(new Vector3(0, 90, 0));
                transformTarget.position = startPosition;
            }
        }
    }
    #endregion
}
